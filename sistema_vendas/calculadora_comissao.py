import math

VALOR_LIMITE_FAIXA = 10000
PERCENTUAL_FAIXA_INFERIOR = 0.05
PERCENTUAL_FAIXA_SUPERIOR = 0.06

def deixar_so_com_duas_casas_decimais(valor):
    return math.floor(valor * 100) / 100

def calcula_comissao(venda):
    if (venda <= VALOR_LIMITE_FAIXA):
        comissao = venda * PERCENTUAL_FAIXA_INFERIOR
    else:
        comissao = venda * PERCENTUAL_FAIXA_SUPERIOR

    return deixar_so_com_duas_casas_decimais(comissao)
